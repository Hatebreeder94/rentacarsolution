﻿using System.Linq;

namespace RentACar.DAL.RepositoryInterface
{
    //TODO[http://www.asp.net/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application]
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        void Create(T data);
        void Update(T data);
        void Delete(T data);
        
    }
}
