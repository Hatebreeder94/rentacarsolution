﻿using System.Data.Entity;
using RentACar.DAL.Models;
namespace RentACar.DAL.DbContext
{
    public class RentACarDbContext : System.Data.Entity.DbContext
    {
        public RentACarDbContext() : base("RentACarDbContext") { }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

    }
}
