﻿using RentACar.DAL.Models;
using System.Collections.Generic;

namespace RentACar.BLL.ServiceInterface
{
    public interface ICarService
    {
        IEnumerable<Car> GetAll();
        Car GetById(int id);
        void Create(Car data);
        void Update(Car data);
        void Delete(Car data);

    }
}
