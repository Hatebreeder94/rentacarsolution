﻿using RentACar.DAL.Models;
using System.Collections.Generic;

namespace RentACar.BLL.ServiceInterface
{
    public interface IUserService
    {
        IEnumerable<User> GetAll();
        User GetById(int id);
        User Login(string login, string password);
        User Register(User user);
        IEnumerable<Role> GetRoles();

    }
}
