﻿using RentACar.DAL.Models;
using System.Collections.Generic;

namespace RentACar.BLL.ServiceInterface
{
    public interface IPortfolioService
    {
        IEnumerable<Portfolio> GetAll();
        Portfolio GetById(int id);
        void Create(Portfolio data);
        void Update(Portfolio data);
        void Delete(Portfolio data);
    }
}
