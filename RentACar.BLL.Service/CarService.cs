﻿using RentACar.BLL.ServiceInterface;
using RentACar.DAL.Models;
using RentACar.DAL.RepositoryInterface;
using System.Collections.Generic;
using System.Linq;

namespace RentACar.BLL.Service
{
    public class CarService : ICarService
    {
        private IUnitOfWork _unitOfWork;
        private IRepository<Car> _repository;

        public CarService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = unitOfWork.Repository<Car>();
        }

        public IEnumerable<Car> GetAll()
        {
            var data = _repository.GetAll();
            return data;
        }

        public Car GetById(int id)
        {
            var data = _repository.GetAll().FirstOrDefault(x => x.Id == id);
            return data;
        }

        public void Create(Car data)
        {
            _repository.Create(data);
            _unitOfWork.SaveChanges();
        }

        public void Update(Car data)
        {
            _repository.Update(data);
            _unitOfWork.SaveChanges();
        }

        public void Delete(Car data)
        {
            _repository.Delete(data);
            _unitOfWork.SaveChanges();
        }
    }
}
