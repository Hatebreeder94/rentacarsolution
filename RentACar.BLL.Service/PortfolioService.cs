﻿using RentACar.BLL.ServiceInterface;
using RentACar.DAL.Models;
using RentACar.DAL.RepositoryInterface;
using System.Collections.Generic;
using System.Linq;

namespace RentACar.BLL.Service
{
    public class PortfolioService : IPortfolioService
    {
        private IUnitOfWork _unitOfWork;
        private IRepository<Portfolio> _repository;

        public PortfolioService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = unitOfWork.Repository<Portfolio>();
        }

        public IEnumerable<Portfolio> GetAll()
        {
            var data = _repository.GetAll();
            return data;
        }

        public Portfolio GetById(int id)
        {
            var data = _repository.GetAll().FirstOrDefault(x => x.Id == id);
            return data;
        }

        public void Create(Portfolio data)
        {
            _repository.Create(data);
            _unitOfWork.SaveChanges();
        }

        public void Update(Portfolio data)
        {
            _repository.Update(data);
            _unitOfWork.SaveChanges();
        }

        public void Delete(Portfolio data)
        {
            _repository.Delete(data);
            _unitOfWork.SaveChanges();
        }
    }
}
