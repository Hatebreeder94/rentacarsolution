﻿using RentACar.BLL.ServiceInterface;
using RentACar.DAL.Models;
using RentACar.DAL.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.BLL.Service
{
    public class UserService : IUserService
    {
        private IUnitOfWork _unitOfWork;
        private IRepository<User> _userRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<UserRole> _userRoleRepository;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _userRepository = unitOfWork.Repository<User>();
            _roleRepository = unitOfWork.Repository<Role>();
            _userRoleRepository = unitOfWork.Repository<UserRole>();
        }

        public IEnumerable<User> GetAll()
        {
            var data = _userRepository.GetAll();
            return data;
        }

        public User GetById(int id)
        {
            var data = _userRepository.GetAll().FirstOrDefault(x => x.Id == id);
            return data;
        }

        public User Login(string login, string password)
        {
            //TODO: add login logic
            var data = new User();
            return data;
        }

        public User Register(User data)
        {
            //TODO: add register logic
            return data;
        }

        public IEnumerable<Role> GetRoles()
        {
            var data = _roleRepository.GetAll();
            return data;
        }
    }
}
