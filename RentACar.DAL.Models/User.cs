﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace RentACar.DAL.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(30)]
        public string Login { get; set; }
        [Required]
        public string PasswordHash { get; set; }
        public virtual ICollection<Role> Roles { get; set; }    
    }
}
