﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentACar.DAL.Models
{
    public class Car
    {
        [Key]
        public int Id { get; set; }        
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public bool Rented { get; set; }
        public DateTime? RentTime { get; set; }

        public int ProtfolioId { get; set; }
        [ForeignKey("PrtfolioId")]
        public Portfolio Portfolio { get; set; }
    }
}
