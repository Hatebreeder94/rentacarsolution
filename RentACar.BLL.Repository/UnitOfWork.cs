﻿using System;
using System.Data.Entity;
using RentACar.DAL.RepositoryInterface;

namespace RentACar.BLL.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        internal DbContext Context;
        private DbContextTransaction _transaction;

        public UnitOfWork(DbContext context)
        {
            Context = context;
        }      
        public IRepository<T> Repository<T>() where T : class
        {
            var repositoryType = typeof(Repository<>);

            return (IRepository<T>)Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), Context);
        }
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }
        public void Dispose()
        {
           this.Dispose();
        }
    }
}
